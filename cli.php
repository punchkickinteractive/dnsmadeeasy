<?php

require_once dirname(__DIR__) . '/dnsMadeEasy/DnsMadeEasy.php';

function arguments($args)
{
  array_shift($args);
  $endofoptions = false;

  $ret = array
      (
      'commands'  => array(),
      'options'   => array(),
      'flags'     => array(),
      'arguments' => array(),
  );

  while ($arg = array_shift($args))
  {

    // if we have reached end of options,
    //we cast all remaining argvs as arguments
    if ($endofoptions)
    {
      $ret['arguments'][] = $arg;
      continue;
    }

    // Is it a command? (prefixed with --)
    if (substr($arg, 0, 2) === '--')
    {

      // is it the end of options flag?
      if (!isset($arg[3]))
      {
        $endofoptions = true;
        // end of options;
        continue;
      }

      $value = "";
      $com   = substr($arg, 2);

      // is it the syntax '--option=argument'?
      if (strpos($com, '='))
      {
        list($com, $value) = explode("=", $com, 2);
      }

      // is the option not followed by another option but by arguments
      elseif (isset($args[0]) && strpos($args[0], '-') !== 0)
      {
        while (isset($args[0]) && strpos($args[0], '-') !== 0)
        {
          $value .= array_shift($args) . ' ';
        }
        $value = rtrim($value, ' ');
      }

      $ret['options'][$com] = !empty($value) ? $value : true;
      continue;
    }

    // Is it a flag or a serial of flags? (prefixed with -)
    if (substr($arg, 0, 1) === '-')
    {
      for ($i = 1; isset($arg[$i]); $i++)
      {
        $ret['flags'][] = $arg[$i];
      }
      continue;
    }

    // finally, it is not option, nor flag, nor argument
    $ret['commands'][] = $arg;
    continue;
  }

  if (!count($ret['options']) && !count($ret['flags']))
  {
    $ret['arguments'] = array_merge($ret['commands'], $ret['arguments']);
    $ret['commands']  = array();
  }
  return $ret;
}

function processArguments($args)
{
  switch (!(isset($args['commands'][0]) && $args['commands'][0]))
  {
    /**
     * Add a record
     */
    case 'addARecord':
      $subDomain      = null;
      $ip             = null;
      $domain         = null;
      $apiKey         = null;
      $apiSecret      = null;
      $missingOptions = null;

      if (isset($args['options']['subdomain']))
      {
        $subDomain = $args['options']['subdomain'];
      }
      else
      {
        echo "You must provide a 'subdomain'\n";
        $missingOptions = true;
      }

      if (isset($args['options']['ip']))
      {
        $ip = $args['options']['ip'];
      }
      else
      {
        echo "You must provide a 'ip'\n";
        $missingOptions = true;
      }

      if (isset($args['options']['domain']))
      {
        $domain = $args['options']['domain'];
      }
      else
      {
        echo "You must provide a 'domain'\n";
        $missingOptions = true;
      }

      if (isset($args['options']['apiKey']))
      {
        $apiKey = $args['options']['apiKey'];
      }
      else
      {
        echo "You must provide a 'apiKey'\n";
        $missingOptions = true;
      }

      if (isset($args['options']['apiSecret']))
      {
        $apiSecret = $args['options']['apiSecret'];
      }
      else
      {
        echo "You must provide a 'apiSecret'\n";
        $missingOptions = true;
      }

      if ($missingOptions == true)
      {
        exit(1);
      }

      $record = array(
          'name' => $args['options']['subdomain'],
          'type' => 'A',
          'data' => $args['options']['ip'],
          'ttl'  => 3600,
      );

      $dme    = new DnsMadeEasy($apiKey, $apiSecret, false);
      $result = $dme->records->add($domain, $record);
      if ($errors = $result->errors())
      {
        print_r($errors);
        exit(1);
      }
      else
      {
        // grab the JSON decoded results.
        // use TRUE to return an associative array, FALSE to return an object.
        $record = $result->body(false);

        // output the assigned record ID
        print_r($record->id);
      }

      break;
    default:
      echo "This CLI tool is, by design, not complete.\n";
      echo "Maybe give 'addARecord' a try.\n\n";
      break;
  }
}

processArguments(arguments($argv));
exit(0);
